# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
$ ->

  $('.symbol_autocomplete').autocomplete
    minLength: 1
    source: (request, response) ->
      $.ajax
        type: "GET"
        dataType: "jsonp"
        jsonp: "callback"
        jsonpCallback: "yahooCallback"
        data:
          query: request.term,
          region: "US",
          lang: "en-US"
        cache: true,
        url: "https://s.yimg.com/aq/autoc"


      `yahooCallback = function (data) {
          response($.map(data.ResultSet.Result, function (item) {
              return {
                  label: item.symbol + ' ' + item.name + '   ' + item.typeDisp + ' - ' + item.exchDisp,
                  value: item.symbol,
                  name: item.name
              }
          }));
      }`

    select: (event, ui) ->
      $(".symbol_autocomplete").val ui.item.value
      $('#stock_name').val ui.item.name

    open: ->
      $(this).removeClass("ui-corner-all").addClass "ui-corner-top"

    close: ->
      $(this).removeClass("ui-corner-top").addClass "ui-corner-all"






