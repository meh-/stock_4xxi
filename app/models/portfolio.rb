class Portfolio
  include Mongoid::Document
  include Mongoid::Timestamps

  belongs_to :user, index: true
  embeds_many :stocks

  field :name, type: String
  field :description, type: String

  validates :name, presence: true

  def historical_info
    yahoo_client = YahooFinance::Client.new
    end_date = Time.now
    start_date = end_date - 2.years
    summary_data = Hash.new(0)
    stocks.each do |stock|
      hist_data = yahoo_client.historical_quotes(stock.symbol, start_date: start_date, end_date: end_date)
      hist_data.each do |day_info|
        summary_data[day_info.trade_date] += stock.shares_count * day_info.adjusted_close.to_f
      end
    end
    summary_data.map { |date, value| [Time.parse(date).to_i * 1000, value] }.reverse
  end

  def fill_stocks_info
    return [] unless stocks.any?
    yahoo_client = YahooFinance::Client.new
    data = yahoo_client.quotes(
        stocks.map(&:symbol),
        [
            :change, :change_in_percent, :symbol, :high, :low, :last_trade_price,
            :low_52_weeks, :high_52_weeks, :close, :open, :volume
        ]
    )
    data.each do |quote|
      stock = stocks.find_by(symbol: quote.symbol)
      stock.update_attributes(
          change: quote.change,
          change_in_percent: quote.change_in_percent,
          high: quote.high,
          low: quote.low,
          last_trade_price: quote.last_trade_price,
          low_52_weeks: quote.low_52_weeks,
          high_52_weeks: quote.high_52_weeks,
          close: quote.close,
          open: quote.open,
          volume: quote.volume
      ) if stock
    end
    stocks
  end

end
