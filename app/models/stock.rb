class Stock
  include Mongoid::Document
  include Mongoid::Timestamps

  embedded_in :portfolio

  attr_accessor :change, :change_in_percent, :high, :low, :last_trade_price, :low_52_weeks, :high_52_weeks, :close, :open, :volume

  field :symbol, type: String
  field :name, type: String
  field :shares_count, type: Integer

  index({ symbol: 1 }, { unique: true })

  validates :symbol, presence: true, uniqueness: true
  validates :shares_count, numericality: { greater_than: 0 }

  def fill_quote_info
    yahoo_client = YahooFinance::Client.new
    data = yahoo_client.quote(
        symbol,
        [
            :change, :change_in_percent, :symbol, :high, :low, :last_trade_price,
            :low_52_weeks, :high_52_weeks, :close, :open, :volume
        ]
    )
    self.update_attributes(
        change: data.change,
        change_in_percent: data.change_in_percent,
        high: data.high,
        low: data.low,
        last_trade_price: data.last_trade_price,
        low_52_weeks: data.low_52_weeks,
        high_52_weeks: data.high_52_weeks,
        close: data.close,
        open: data.open,
        volume: data.volume
    )
  end

  def historical_info
    yahoo_client = YahooFinance::Client.new
    end_date = Time.now
    start_date = end_date - 2.years
    summary_data = Hash.new(0)

    hist_data = yahoo_client.historical_quotes(symbol, start_date: start_date, end_date: end_date)
    hist_data.each do |day_info|
      summary_data[day_info.trade_date] += shares_count * day_info.adjusted_close.to_f
    end
    summary_data.map { |date, value| [Time.parse(date).to_i * 1000, value] }.reverse
  end
end
