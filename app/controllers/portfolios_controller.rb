class PortfoliosController < ApplicationController
  before_action :find_portfolio, only: [:show, :edit, :update, :destroy]

  def index
    @portfolios = current_user.portfolios
  end

  def show
    @portfolio.fill_stocks_info
    chart_data = @portfolio.historical_info
    @chart = LazyHighCharts::HighChart.new('line') do |f|
      f.title(text: "Total #{@portfolio.name } portfolio price, $")
      f.series(name: 'price in $', data: chart_data, tooltip: { valueDecimals: 2 })
      f.rangeSelector(selected: 1)
    end
  end

  def new
    @portfolio = current_user.portfolios.new
  end

  def edit
  end

  def create
    @portfolio = current_user.portfolios.new(portfolio_params)

    respond_to do |format|
      if @portfolio.save
        format.html { redirect_to @portfolio, notice: 'Portfolio was successfully created.' }
        format.json { render :show, status: :created, location: @portfolio }
      else
        format.html { render :new }
        format.json { render json: @portfolio.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @portfolio.update(portfolio_params)
        format.html { redirect_to @portfolio, notice: 'Portfolio was successfully updated.' }
        format.json { render :show, status: :ok, location: @portfolio }
      else
        format.html { render :edit }
        format.json { render json: @portfolio.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @portfolio.destroy
    respond_to do |format|
      format.html { redirect_to portfolios_url, notice: 'Portfolio was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

  def find_portfolio
    @portfolio = current_user.portfolios.find(params[:id])
  end

  def portfolio_params
    params.require(:portfolio).permit(:name, :description)
  end
end
