class StocksController < ApplicationController
  before_action :find_portfolio
  before_action :find_stock, only: [:show, :edit, :update, :destroy]

  def show
    @stock.fill_quote_info
    data = @stock.historical_info
    @chart = LazyHighCharts::HighChart.new('line') do |f|
      f.title(text: "#{@stock.symbol} chart")
      f.series(name: 'price in $', data: data, tooltip: { valueDecimals: 2 })
      f.rangeSelector(selected: 1)
    end
  end

  def new
    @stock = @portfolio.stocks.new
  end

  def edit
  end

  def create
    @stock = @portfolio.stocks.new(stock_params)

    respond_to do |format|
      if @stock.save
        format.html { redirect_to portfolio_stock_path(@portfolio, @stock), notice: 'Stock was successfully created.' }
        format.json { render :show, status: :created, location: [@portfolio, @stock] }
      else
        format.html { render :new }
        format.json { render json: @stock.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @stock.update(stock_params)
        format.html { redirect_to portfolio_stock_path(@portfolio, @stock), notice: 'Stock was successfully updated.' }
        format.json { render :show, status: :ok, location: [@portfolio, @stock] }
      else
        format.html { render :edit }
        format.json { render json: @stock.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @stock.destroy
    respond_to do |format|
      format.html { redirect_to portfolio_path(@portfolio), notice: 'Stock was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

  def find_portfolio
    @portfolio = current_user.portfolios.find(params[:portfolio_id])
  end

  def find_stock
    @stock = @portfolio.stocks.find(params[:id])
  end

  def stock_params
    params.require(:stock).permit(:symbol, :name, :shares_count)
  end
end
