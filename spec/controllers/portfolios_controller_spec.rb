require 'rails_helper'

RSpec.describe PortfoliosController, type: :controller do
  login_user

  let(:valid_attributes) {
    { name: Faker::Company.buzzword, description: Faker::Company.bs, user_id: subject.current_user.id }
  }

  let(:invalid_attributes) {
    { description: Faker::Lorem.words(3) }
  }


  it "should have a current_user" do
    subject.current_user.should_not be_nil
  end

  describe "GET #index" do
    it "assigns all portfolios as @portfolios" do
      portfolio = Portfolio.create! valid_attributes
      get :index, {}
      expect(assigns(:portfolios)).to eq([portfolio])
    end
  end

  describe "GET #show" do
    it "assigns the requested portfolio as @portfolio" do
      portfolio = Portfolio.create! valid_attributes
      get :show, { :id => portfolio.to_param }
      expect(assigns(:portfolio)).to eq(portfolio)
    end
  end

  describe "GET #new" do
    it "assigns a new portfolio as @portfolio" do
      get :new, {}
      expect(assigns(:portfolio)).to be_a_new(Portfolio)
    end
  end

  describe "GET #edit" do
    it "assigns the requested portfolio as @portfolio" do
      portfolio = Portfolio.create! valid_attributes
      get :edit, { :id => portfolio.to_param }
      expect(assigns(:portfolio)).to eq(portfolio)
    end
  end

  describe "POST #create" do
    context "with valid params" do
      it "creates a new Portfolio" do
        expect {
          post :create, { :portfolio => valid_attributes }
        }.to change(Portfolio, :count).by(1)
      end

      it "assigns a newly created portfolio as @portfolio" do
        post :create, { :portfolio => valid_attributes }
        expect(assigns(:portfolio)).to be_a(Portfolio)
        expect(assigns(:portfolio)).to be_persisted
      end

      it "redirects to the created portfolio" do
        post :create, { :portfolio => valid_attributes }
        expect(response).to redirect_to(Portfolio.last)
      end
    end

    context "with invalid params" do
      it "assigns a newly created but unsaved portfolio as @portfolio" do
        post :create, { :portfolio => invalid_attributes }
        expect(assigns(:portfolio)).to be_a_new(Portfolio)
      end

      it "re-renders the 'new' template" do
        post :create, { :portfolio => invalid_attributes }
        expect(response).to render_template("new")
      end
    end
  end

  describe "PUT #update" do
    context "with valid params" do
      let(:new_attributes) {
        {name: Faker::Lorem.words(2), description: Faker::Lorem.words(5)}
      }

      it "updates the requested portfolio" do
        portfolio = Portfolio.create! valid_attributes
        put :update, { :id => portfolio.to_param, :portfolio => new_attributes }
        portfolio.reload
        expect(portfolio).to be_valid
      end

      it "assigns the requested portfolio as @portfolio" do
        portfolio = Portfolio.create! valid_attributes
        put :update, { :id => portfolio.to_param, :portfolio => valid_attributes }
        expect(assigns(:portfolio)).to eq(portfolio)
      end

      it "redirects to the portfolio" do
        portfolio = Portfolio.create! valid_attributes
        put :update, { :id => portfolio.to_param, :portfolio => valid_attributes }
        expect(response).to redirect_to(portfolio)
      end
    end

    context "with invalid params" do
      it "assigns the portfolio as @portfolio" do
        portfolio = Portfolio.create! valid_attributes
        put :update, { :id => portfolio.to_param, :portfolio => invalid_attributes }
        expect(assigns(:portfolio)).to eq(portfolio)
      end

    end
  end

  describe "DELETE #destroy" do
    it "destroys the requested portfolio" do
      portfolio = Portfolio.create! valid_attributes
      expect {
        delete :destroy, { :id => portfolio.to_param }
      }.to change(Portfolio, :count).by(-1)
    end

    it "redirects to the portfolios list" do
      portfolio = Portfolio.create! valid_attributes
      delete :destroy, { :id => portfolio.to_param }
      expect(response).to redirect_to(portfolios_url)
    end
  end

end
