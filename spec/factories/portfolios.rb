FactoryGirl.define do
  factory :portfolio do
    name { Faker::Company.buzzword }
    description { Faker::Company.bs }
  end

end
