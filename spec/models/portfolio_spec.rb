require 'rails_helper'

RSpec.describe Portfolio, type: :model do
  it 'has a valid factory' do
    expect(FactoryGirl.build(:portfolio)).to be_valid
  end

  it 'is invalid without a name' do
    expect(FactoryGirl.build(:portfolio, name: nil)).to_not be_valid
  end
end
